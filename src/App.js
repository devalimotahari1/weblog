import "./styles/index.css";
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import {Toaster} from "react-hot-toast";
import UserProvider from "./context/user/provider";
import AuthProvider from "./providers/AuthProvider";

function App() {
    return (
        <BrowserRouter>
            <Toaster position="top-center" reverseOrder={false}/>
            <UserProvider>
                <AuthProvider>
                    <AppRouter/>
                </AuthProvider>
            </UserProvider>
        </BrowserRouter>
    );
}

export default App;
