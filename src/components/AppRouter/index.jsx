import React, {useCallback} from "react";
import {Route, Routes} from "react-router-dom";
import routes from "./routes";
import ScrollToTop from "../ScrollToTop";

const AppRouter = () => {
    const handleRoutes = useCallback((routes) => {
        return routes?.map((route, key) => (
            <Route key={key} {...route}>
                {handleRoutes(route.children)}
            </Route>
        ));
    }, []);

    return (
        <ScrollToTop>
            <Routes>
                {handleRoutes(routes)}
            </Routes>
        </ScrollToTop>
    )
};

export default AppRouter;
