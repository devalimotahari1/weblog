import BlogItem from "../../routes/BlogItem";
import NotFound from "../../routes/NotFound";
import Layout from "../Layout";
import SignInPage from "../../routes/Sign/SignIn";
import SignUpPage from "../../routes/Sign/SignUp";
import Home from "../../routes/Home";
import SigninChecker from "../Layout/SigninChecker";
import PanelLayout from "../Layout/Panel";
import ForgetPassword from "../../routes/Sign/ForgetPassword";
import ResetPassword from "../../routes/Sign/ResetPassword";
import About from "../../routes/About";
import Dashboard from "../../routes/Panel/Dashboard";

const layoutRoutes = [
    {
        path: "/",
        element: <Home/>,
        exact: true,
    },
    {
        path: "/about",
        element: <About/>,
    },
    {
        path: "/post/:postId",
        element: <BlogItem/>,
    },
];
const signRoutes = [
    {
        path: "/signin",
        element: <SignInPage/>,
    },
    {
        path: "/signup",
        element: <SignUpPage/>,
    },
    {
        path: "/forget-password",
        element: <ForgetPassword/>,
    },
    {
        path: "/reset-password/:token",
        element: <ResetPassword/>,
    },
];

const panelRoutes = [
    {
        path: "/panel/dashboard",
        element: <Dashboard/>,
    },
    {
        path: "/panel/profile",
        element: <>Profile</>,
    },
    {
        path: "/panel/blogs",
        element: <>blogs</>,
    },
]

const routes = [
    {
        element: <Layout/>,
        children: layoutRoutes,
    },
    {
        element: <SigninChecker/>,
        children: signRoutes,
    },
    {
        element: <PanelLayout/>,
        children: panelRoutes
    },
    {
        path: "*",
        element: <NotFound/>,
    },
];

export default routes;
