import Banner from "./BlogParts/Banner";
import TitleBlog from "./BlogParts/Title";
import {Link} from "react-router-dom";
import BlogCardLoader from "../BlogCardLoader";

const BlogCard = ({blogItem}) => {

    if (!blogItem) {
        return <BlogCardLoader className={"mb-10"}/>
    }

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 my-10">
            <div className="">
                <Link to={`/post/${blogItem._id}`}>
                    <Banner thumbnail={blogItem.thumbnail}/>
                </Link>
            </div>
            <div className="md:mt-[20px]">
                <TitleBlog blogItem={blogItem}/>
            </div>
        </div>
    );
};
export default BlogCard;
