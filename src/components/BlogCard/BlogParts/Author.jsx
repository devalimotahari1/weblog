import {toJalali} from "../../../utils";

const Author = ({fullName, date}) => {
    return (
        <div className="flex flex-row w-70">
            <img
                className="w-10 h-10 rounded-[10px]"
                src="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
                alt="Rounded avatar"
                loading={"lazy"}
            />
            <div className="flex flex-col justify-between mr-[10px]">
                <p className="sm:text-xs text-sm font-[500] text-[#182333]">{fullName}</p>
                <p className="text-xs font-[500] text-[#7F8084]">{toJalali(date)}</p>
            </div>
        </div>
    );
};

export default Author;
