import Image from "../../Image";

const Banner = ({thumbnail}) => {
    return (
        <div className="rounded-md">
            <Image src={thumbnail} alt={"Banner Image"} className={"rounded-[30px] w-full md:w-3/4 h-72 object-cover mx-auto"}/>
        </div>
    );
};

export default Banner;
