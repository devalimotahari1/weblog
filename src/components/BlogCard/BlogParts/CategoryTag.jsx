const CategoryTag = ({tagTitle = "بدون دسته بندی"}) => {
    return (
        <button className="bg-[#ECE7FE] py-[4px] px-[10px] rounded-lg">
            <p className="text-xs color-[#8B7AB5]">{tagTitle}</p>
        </button>
    );
};

export default CategoryTag;
