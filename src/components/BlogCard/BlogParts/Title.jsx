import Author from "./Author";
import CategoryTag from "./CategoryTag";
import {Link} from "react-router-dom";

const TitleBlog = ({blogItem}) => {
    const {_id, title, body, createdAt, user: {fullname}} = blogItem;
    return (
        <div className="md:flex md:flex-col md:justify-between md:h-full mt-5 md:mt-0 md:mx-[40px]">
            <div className="mb-[5px]">
                <CategoryTag/>
            </div>
            <Link to={`/post/${_id}`}>
                <p className="md:my-5 text-xl md:text-4xl font-bold text-[#182333]">
                    {title}
                </p>
            </Link>
            <p dangerouslySetInnerHTML={{__html: body}} className="truncate truncate-2 text-base mt-[10px] font-light text-[#A7A7A8]"/>
            <div className="mt-[20px]">
                <Author date={createdAt} fullName={fullname}/>
            </div>
        </div>
    );
};

export default TitleBlog;
