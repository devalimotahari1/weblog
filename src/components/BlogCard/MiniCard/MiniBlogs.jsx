import React from "react";
import MiniCard from "./MiniCard";
import BlogLoader from "../../BlogItem/BlogLoader";

const MiniBlogs = ({blogs}) => {

    return (
        <div className={"grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-10 mt-10"}>
            {!blogs && [1, 2, 3, 4].map((v, k) =>
                <BlogLoader key={k} size={"mini"}/>
            )}
            {blogs?.map((blog, index) =>
                index !== 0 &&
                <MiniCard
                    key={blog._id}
                    blogItem={blog}
                />
            )}
        </div>
    );
};
export default MiniBlogs;
