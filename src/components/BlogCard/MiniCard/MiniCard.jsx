import React from "react";
import {Link} from "react-router-dom";
import Author from "../BlogParts/Author";
import CategoryTag from "../BlogParts/CategoryTag";
import Image from "../../Image";

const MiniCard = ({blogItem}) => {
    const {_id, thumbnail, tag = "بدون دسته بندی", title, body, createdAt, user: {fullname}} = blogItem;

    return (
        <>
            <div className="w-full relative flex flex-col justify-between h-full">
                <Link to={`/post/${_id}`}>
                    <Image src={thumbnail} alt={title} className="w-full h-72 object-cover rounded-3xl"/>
                </Link>
                <div className="mt-[20px]">
                    <CategoryTag tagTitle={tag}/>
                </div>
                <div className="mt-[5px]">
                    <Link to={`/post/${_id}`}>
                        <p className="text-xl font-extrabold text-[#182333]">
                            {title}
                        </p>
                    </Link>
                    <p dangerouslySetInnerHTML={{__html: body}} className="truncate text-sm mt-5 text-justify font-light text-[#A7A7A8]"/>
                </div>
                <div className="mt-5">
                    <Author date={createdAt} fullName={fullname}/>
                </div>
            </div>
        </>
    )
        ;
};

export default MiniCard;
