import React from "react"
import ContentLoader from "react-content-loader"

const BlogCardLoader = (props) => (
    <ContentLoader
        speed={2}
        width={"100%"}
        height={300}
        viewBox="0 0 600 150"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
    >
        <rect x="29" y="3" rx="0" ry="0" width="338" height="20"/>
        <rect x="784" y="151" rx="0" ry="0" width="981" height="38"/>
        <rect x="3" y="55" rx="0" ry="0" width="363" height="6"/>
        <rect x="3" y="67" rx="0" ry="0" width="363" height="6"/>
        <rect x="3" y="80" rx="0" ry="0" width="363" height="6"/>
        <rect x="339" y="128" rx="4" ry="4" width="25" height="22"/>
        <rect x="284" y="129" rx="0" ry="0" width="50" height="6"/>
        <rect x="289" y="142" rx="0" ry="0" width="44" height="6"/>
        <rect x="378" y="2" rx="10" ry="10" width="199" height="148"/>
    </ContentLoader>
)

export default BlogCardLoader;