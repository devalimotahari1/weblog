import React from 'react';
import Icon from "../Icon";

const TextIcon = ({icon, text}) => {
    return (
        <div className="flex justify-between items-center">
            <Icon name={icon} color={"#a211d3"} size={18}/>
            <span className={"text-sm font-[200] md:font-light mr-1"}>{text}</span>
        </div>
    );
};
export default TextIcon;