import React from "react";

const Input = ({type, placeholder, change, value, name}) => {
    return (
        <>
            <div>
                <label htmlFor={name} className="sr-only">
                    {placeholder}
                </label>
                <input
                    onChange={change}
                    value={value}
                    name={name}
                    type={type}
                    required
                    className="appearance-none block w-full bg-gray-100 text-gray-700 border  rounded-xl py-4 px-5 mb-3 leading-tight focus:outline-none focus:bg-white"
                    placeholder={placeholder}
                />
            </div>
        </>
    );
};

export default Input;
