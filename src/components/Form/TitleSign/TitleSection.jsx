import React from "react";
import {Link} from "react-router-dom";
import Icon from "../../Icon";

const TitleSign = ({
                       Title = "اینجا هر کسی می‌تونه بنویسه!",
                       subTitle = "همین حالا حساب کاربری خودت را بساز و دوران جدید وبلاگ نویسی را شروع کن.",
                   }) => {
    return (
        <>
            <Link to={"/"} className={"mb-2 lt flex justify-end"}><Icon name={"home"}/></Link>
            <div>
                <Link to={"/"}>
                    <img
                        className="mx-auto h-12 w-auto"
                        src={"/logo.png"}
                        alt={"logo"}
                    />
                </Link>
                <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                    {Title}
                </h2>
                <p className="mt-2 text-center text-sm text-gray-600">{subTitle}</p>
            </div>
        </>
    );
};
export default TitleSign;
