import React from 'react';

const Button = ({variant = "primary", disabled, className, children, ...other}) => {
    return (
        <button
            {...other}
            disabled={disabled}
            className={`button-${variant} ${disabled ? "opacity-50" : ""} ${className}`}
        >
            {children}
        </button>
    );
};
export default Button;