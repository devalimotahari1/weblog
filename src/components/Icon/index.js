import React from "react";
import Icons from "assets/icons/icons.svg";


const Icon = ({className = "", name = "menu", color = "transparent", size = 24, ...other}) => (
    <svg {...other} className={`icon icon-${name} ${className}`} fill={color} width={size} height={size}>
        <use xlinkHref={`${Icons}#icon-${name}`}/>
    </svg>
);

export default Icon;