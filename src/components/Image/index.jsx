import React from 'react';
import {http} from "../../services/httpService";

const Image = ({src, alt, className, notFoundSrc = "/not-found.webp"}) => {
    return (
        <object data={`${http.BASE_URL}/uploads/thumbnails/${src}`}
                type="image/png"
                className={className}
        >
            <img
                className={`!object-contain ${className ?? ""}`}
                src={notFoundSrc}
                alt={alt}
            />
        </object>
    );
};
export default Image;