import React from "react";
import {Link} from "react-router-dom";

const Footer = () => {
  return (
      <>
        <footer className="p-4 bg-white shadow-2xl shadow-gray-500 z-10 flex text-center flex-col md:flex-row md:items-center md:justify-between md:p-6">
        <span className="text-sm text-gray-500 sm:text-center">
          © {new Date().getFullYear()} | تمامی حقوق محفوظ است.
        </span>
          <ul className="flex flex-wrap justify-center items-center mt-3 text-sm text-gray-500 md:mt-0">
            <li>
              <Link to="/about" className="mr-4 hover:underline md:mr-6 ">
                درباره ما
              </Link>
            </li>
            <li>
              <Link to="#" className="mr-4 hover:underline md:mr-6">
                حریم خصوصی
              </Link>
            </li>
        </ul>
      </footer>
    </>
  );
};
export default Footer;
