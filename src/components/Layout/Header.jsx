import React, {useCallback, useState} from "react";
import {Link, NavLink} from "react-router-dom";
import {NAVIGATIONS} from "./navs";
import Icon from "../Icon";
import SignBtn from "./SignBtn";
import {useUserStore} from "../../context/user/provider";
import MenuButton from "../MenuButton";

const MobileNav = ({handleClose}) => {
    return (
        <>
            <div className="md:hidden p-2 flex flex-col w-full bg-white shadow-md absolute top-20 left-0">
                {NAVIGATIONS.map((nav, key) => (
                    <NavLink
                        key={key}
                        to={nav.href}
                        className={({isActive}) =>
                            `p-1.5 px-3 w-full rounded-lg text-right hover:bg-gray-100 transition ${
                                isActive
                                    ? "text-gray-900 font-medium"
                                    : "text-gray-500 font-[450]"
                            }`
                        }
                        onClick={handleClose}
                    >
                        {nav.title}
                    </NavLink>
                ))}
            </div>
        </>
    );
};

const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
    const {fullName} = useUserStore();

    const handleMenu = useCallback(() => {
        setIsOpen((p) => !p);
    }, []);

    return (
        <>
            <header className="sticky bg-white z-50 flex flex-row-reverse md:flex-row justify-between items-center top-0 px-3.5 py-5 shadow-md shadow-gray-100">
                <img
                    className="h-full w-[40px] md:w-10"
                    src={"/logo.png"}
                    alt={"logo"}
                />
                <nav className="hidden md:flex justify-center align-middle items-center gap-10">
                    {NAVIGATIONS.map((nav, key) => (
                        <NavLink
                            key={key}
                            to={nav.href}
                            className={({isActive}) =>
                                `py-1.5 hover:text-black transition ${
                                    isActive
                                        ? "text-gray-900 font-medium"
                                        : "text-gray-500 font-[450]"
                                }`
                            }
                        >
                            {nav.title}
                        </NavLink>
                    ))}
                </nav>
                {isOpen && <MobileNav handleClose={handleMenu}/>}
                <SignBtn className={"hidden md:block"}/>
                <div className="block md:hidden flex justify-between items-center">
                    <MenuButton onClick={handleMenu} isOpen={isOpen}/>
                    {
                        fullName ?
                            <Link to={"/panel/dashboard"}>
                                <Icon name={"user-square"} size={22} className='mr-3'/>
                            </Link>
                            :
                            <Link to={"/signin"}>
                                <Icon name={"login"} size={22} className='mr-3'/>
                            </Link>
                    }
                </div>
            </header>
        </>
    );
};
export default Header;
