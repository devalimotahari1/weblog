import React from 'react';
import Icon from "../../Icon";
import {useUserStore} from "../../../context/user/provider";
import MenuButton from "../../MenuButton";

const PanelHeader = ({handleMenu}) => {
    const {logOut} = useUserStore();

    return (
        <header className='sticky top-0 w-full shadow-md shadow-gray-100 py-5 px-3 flex justify-between items-center'>
            <div className="flex justify-between gap-5 items-center">
                <MenuButton onClick={handleMenu} className={"block lg:hidden"}/>
                <span>وبلاگ</span>
            </div>
            <div className="flex justify-between text-sm gap-2 p-2 transition duration-300 rounded hover:bg-gray-50 cursor-pointer" onClick={logOut}>
                <Icon name={"login"} size={20}/>
                <span>خروج</span>
            </div>
        </header>
    );
};
export default PanelHeader;