import React from 'react';
import {Link, useLocation} from "react-router-dom";
import Icon from "../../Icon";

const menu = [
    {title: "داشبورد", link: "/dashboard"},
    {title: "پروفایل", link: "/profile"},
    {title: "پست ها", link: "/blogs"},
]


const PanelSideBar = ({isOpen, handleMenu}) => {
    const {pathname: path} = useLocation();

    return (
        <aside className={`${isOpen ? "right-0" : "-right-[100%]"} transition-all duration-300 h-full absolute lg:relative lg:right-0 top-0 min-w-full lg:min-w-[18%] bg-white shadow-lg text-center pt-20 lg:pt-3 py-3 flex flex-col gap-1`}>
            <Icon name={"close"} className={"inline lg:hidden absolute top-6 right-4"} onClick={handleMenu}/>
            {menu.map(({link, title}) =>
                <Link
                    key={`/panel${link}`}
                    to={`/panel${link}`}
                    className={`hover:bg-gray-100 p-3 transition duration-500 ${`/panel${link}` === path ? "bg-gray-50 rounded w-100 lg:mx-0 lg:border-l-[3px] lg:rounded-none lg:bg-transparent border-cyan-500 font-semibold" : "border-l-0"}`}
                    onClick={handleMenu}
                >
                    {title}
                </Link>
            )}
        </aside>
    );
};
export default PanelSideBar;