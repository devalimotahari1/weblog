import React, {useCallback, useState} from 'react';
import {Navigate, Outlet, useLocation} from 'react-router-dom';
import PanelHeader from "./Header";
import Footer from "../Footer";
import {useUserStore} from "../../../context/user/provider";
import PanelSideBar from "./PanelSideBar";


const PanelLayout = () => {
    const {fullName} = useUserStore();
    const {pathname} = useLocation();

    const [menuIsOpen, setMenuIsOpen] = useState(false);

    const handleMenu = useCallback(() => {
        setMenuIsOpen(p => !p);
    }, [])

    if (!fullName) return <Navigate to={"/signin"} state={{location: pathname}}/>

    return (
        <>
            <PanelHeader handleMenu={handleMenu}/>
            <main className='flex flex-1 h-full flex-row gap-2'>
                <PanelSideBar isOpen={menuIsOpen} handleMenu={handleMenu}/>
                <div className="container mx-auto p-2 md:p-4">
                    <Outlet/>
                </div>
            </main>
            <Footer/>
        </>
    );
};
export default PanelLayout;