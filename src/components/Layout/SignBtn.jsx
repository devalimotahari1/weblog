import React from 'react';
import {Link} from "react-router-dom";
import {useUserStore} from "../../context/user/provider";

const SignBtn = ({className}) => {
    const {fullName} = useUserStore();

    return (
        <div className={`${className}`}>
            {
                fullName ?
                    <Link to={"/panel/dashboard"} className="button-primary !py-2.5">
                        پنل مدیریت
                    </Link>
                    : (
                        <>
                            <Link to={"/signin"} className="button-text md:ml-5">
                                ورود
                            </Link>
                            <Link to={"/signup"} className="button-primary !py-1.5 md:!py-2">
                                ثبت نام
                            </Link>
                        </>
                    )
            }
        </div>
    );
};
export default SignBtn;