import React from 'react';
import {Navigate, Outlet} from "react-router-dom";
import {useUserStore} from "../../context/user/provider";

const SigninChecker = () => {
    const {fullName} = useUserStore();

    if (fullName) return <Navigate to={"/"} replace/>
    return <Outlet/>;
};
export default SigninChecker;