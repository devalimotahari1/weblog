import React from 'react';
import Icon from 'components/Icon';

const MenuButton = ({onClick, className, isOpen = false}) => {
    return (
        <button
            className={`${className ?? ""} p-1 rounded focus:bg-gray-200 hover:cursor-pointer hover:bg-gray-100`}
            onClick={onClick}
        >
            <Icon name={isOpen ? "close" : "menu"}/>
        </button>
    );
};
export default MenuButton;