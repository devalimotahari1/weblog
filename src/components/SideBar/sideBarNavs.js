const sideBarNavs = [
    {text: "داشبورد", link: "/dashboard"},
    {text: "پست ها", link: "/posts"},
    {text: "پروفایل", link: "/profile"},
]

export default sideBarNavs;