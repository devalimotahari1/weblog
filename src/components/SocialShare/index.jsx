import React from 'react';
import Icon from "../Icon";

const SocialShare = ({icon, size = 15, link = "#", className, ...other}) => {
    return (
        <a {...other}
           href={link}
           target={"_blank"}
           className={`${className ?? ""} bg-white md:bg-transparent cursor-pointer p-3 rounded-[50%] border border-gray-300 hover:bg-gray-50 active:bg-gray-100 transition duration-300`}
        >
            <Icon name={icon} size={size} color={"#343434"}/>
        </a>
    );
};
export default SocialShare;