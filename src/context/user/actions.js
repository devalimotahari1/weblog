const userActions = {
    SET_USER_FULL_NAME: "SET_USER_FULL_NAME",
    RESET_USER: "RESET_USER",
}
export default userActions;
