import React, {useMemo} from "react";
import userReducer, {userInitialState} from "./reducer";
import userActions from "./actions";
import {useNavigate} from "react-router-dom";
import {http} from "../../services/httpService";

const UserContext = React.createContext(userInitialState);

const UserProvider = ({children}) => {
    const [state, dispatch] = React.useReducer(userReducer, userInitialState);
    const navigate = useNavigate();

    const value = useMemo(() => {
        return {
            fullName: state.fullName,
            setFullName: (fullName) => {
                dispatch({type: userActions.SET_USER_FULL_NAME, fullName});
            },
            reset: () => {
                dispatch({type: userActions.RESET_USER});
            },
            logOut: () => {
                dispatch({type: userActions.RESET_USER});
                http.removeToken();
                navigate.push("/");
            },
        };
    }, [state])

    return (
        <UserContext.Provider value={value}>
            {children}
        </UserContext.Provider>
    );
};
export const useUserStore = () => {
    return React.useContext(UserContext);
}

export default UserProvider;