import userActions from "./actions";

export const userInitialState = {fullName: null}

const userReducer = (state, action) => {
    switch (action.type) {
        case userActions.SET_USER_FULL_NAME:
            return {
                ...state.user,
                fullName: action.fullName,
            };
        case userActions.RESET_USER: {
            return userInitialState;
        }
        default:
            return state;
    }
};

export default userReducer;