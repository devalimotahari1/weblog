import React, {useEffect, useState} from 'react';
import {LoginWithToken} from "../services/UserService";
import {http} from "../services/httpService";
import {useUserStore} from "../context/user/provider";

const AuthProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(true);
    const {setFullName} = useUserStore();

    useEffect(() => {
        const token = http.getToken();
        if (token) {
            setIsLoading(true);
            LoginWithToken(token)
                .then(({data}) => {
                    http.setToken(token);
                    setFullName(data.fullName);
                })
                .catch(err => {
                    http.removeToken();
                })
                .finally(() => setIsLoading(false));
        } else setIsLoading(false);
    }, []);

    return isLoading ? <></> : children;
};
export default AuthProvider;