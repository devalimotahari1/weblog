import React, {useEffect, useMemo, useState} from "react";
import {useParams} from "react-router-dom";
import {getOneBlog} from "../services/BlogService";
import SocialShare from "../components/SocialShare";
import BlogLoader from "../components/BlogItem/BlogLoader";
import TextIcon from "../components/BlogItem/TextIcon";
import {toJalali} from "../utils";
import Image from "../components/Image";

const BlogItem = () => {
    const {postId} = useParams();
    const [postInfo, setPostInfo] = useState();
    const [loading, setLoading] = useState(true);
    const [socialOpen, setSocialOpen] = useState(false);

    useEffect(() => {
        getOneBlog(postId)
            .then(({data}) => setPostInfo(data.post))
            .finally(() => setLoading(false));
    }, []);

    const url = useMemo(() => window.location.href, []);

    if (loading) return <BlogLoader/>;

    if (!postInfo)
        return (
            <div className="h-full flex justify-center items-center">
                <h1 className={"font-bold text-2xl"}>
                    متاسفانه پست مورد نظر شما یافت نشد.
                </h1>
            </div>
        );

    return (
        <>
            <div className="p-4 pt-8 md:p-24 lg:px-64 h-full">
                <h1 className="font-bold text-2xl text-center">{postInfo.title}</h1>
                <div className="flex justify-center gap-6 mt-8">
                    <TextIcon icon={"pen-edit"} text={postInfo.user?.fullname}/>
                    <TextIcon
                        icon={"calendar"}
                        text={toJalali(postInfo.createdAt)}
                    />
                </div>
                <div className="flex justify-center mt-8 mb-8 md:px-8">
                    <Image className={"w-full md:max-h-[500px] md:min-h-[400px] md:min-w-[100%] object-cover rounded-2xl"}
                           src={postInfo.thumbnail}
                           alt={postInfo.title}
                    />
                </div>
                <div className="flex justify-between relative gap-8 md:pl-24 md:pr-8">
                    <SocialShare icon={"share"}
                                 size={20}
                                 onClick={(e) => {
                                     e.preventDefault();
                                     setSocialOpen(p => !p);
                                 }}
                                 className={"fixed md:hidden bottom-7 bg-white z-50"}
                    />
                    <div className={`${socialOpen ? "flex fixed bottom-24 mr-1 z-50" : "hidden"} transition ease-linear duration-300 md:sticky md:top-48 md:flex flex-col gap-3 md:right-0 h-fit`}>
                        <SocialShare icon={"twitter"}
                                     link={`https://twitter.com/share?url=${url}&text=${url}`}
                        />
                        <SocialShare icon={"linkedin"}
                                     link={`https://www.linkedin.com/shareArticle?mini=true&url=${url}&t=${url}`}
                        />
                        <SocialShare icon={"whatsapp"}
                                     link={`whatsapp://send?text=${url}`}
                        />
                        <SocialShare icon={"reddit"}
                                     link={`http://www.reddit.com/submit?url=${url}`}
                        />
                    </div>
                    <div
                        className="flex-1 text-justify text-gray-800"
                        dangerouslySetInnerHTML={{__html: postInfo.body}}
                    />
                </div>
            </div>
        </>
    );
};
export default BlogItem;
