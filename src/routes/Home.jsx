import React, {useEffect, useState} from "react";
import MiniBlogs from "../components/BlogCard/MiniCard/MiniBlogs";
import BlogCard from "../components/BlogCard/BlogCard";
import {toast} from "react-hot-toast";
import getAllBlog from "../services/BlogService";

const Home = () => {
    const [blogs, setBlogs] = useState();

    useEffect(() => {
        getAllBlog()
            .then(({data}) => {
                setBlogs(data.posts);
                console.log("posts: ", data.posts)
            })
            .catch((err) => {
                toast.error(err?.response?.data?.message);
            });
    }, []);


    return (
        <div className="px-8 xl:px-28 container mx-auto my-10">
            <BlogCard blogItem={blogs && blogs[0]}/>
            <hr/>
            <MiniBlogs blogs={blogs}/>
        </div>
    );
};
export default Home;
