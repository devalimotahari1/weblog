import {Link, useLocation} from "react-router-dom";

const NotFound = () => {
    const {pathname} = useLocation();

    return (
        <div className={"flex flex-col justify-center items-center gap-4 h-full h-screen"}>
            <h1 className={"font-bold"}>Page Not Found</h1>
            <Link to={pathname.includes("/panel") ? "/panel/dashboard" : "/"}>
                <button className={"button-primary"}>Go Home</button>
            </Link>
        </div>
    )
}
export default NotFound;