import React from 'react';
import {useUserStore} from "../../context/user/provider";

const Dashboard = (props) => {
    const {fullName} = useUserStore();

    return (
        <div className="flex flex-col gap-5 mt-5 text-lg text-justify h-full items-center">
            <p className="text-center">{`${fullName} `} عزیز، به پنل مدیریت وبلاگ خوش آمدید.</p>
            <p className="text-center">می توانید با استفاده از گزینه های منو، از امکانات پنل استفاده نمایید.</p>
        </div>
    );
};
export default Dashboard;