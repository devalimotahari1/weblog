import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import Input from "../../components/Form/Input";
import TitleSign from "../../components/Form/TitleSign/TitleSection";
import toast from "react-hot-toast";
import Button from "../../components/Form";
import {UserForgetPassword} from "../../services/UserService";


const ForgetPassword = () => {
    const [userData, setUserData] = useState({email: null});
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const changeHandler = (e) => {
        setUserData({...userData, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!Object.values(userData).every(Boolean)) return toast.error("لطفا تمامی موارد را تکمیل نمایید.");
        setIsLoading(true);
        const promise = UserForgetPassword(userData);
        toast.promise(promise, {
            loading: 'در حال انجام عملیات ...',
            success: ({data}) => {
                navigate("/", {replace: true});
                return "لینک بازیابی کلمه عبور به ایمیل شما ارسال شد.";
            },
            error: (err) => `${err?.response?.data?.message ?? "عملیات انجام نشد."}`,
        },).finally(() => setIsLoading(false))
    };

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <TitleSign subTitle={"برای بازیابی رمز عبور لطفا ایمیل خود را وارد نمایید."}/>
                    </div>
                    <form className="w-full max-w-lg">
                        <div className="flex flex-wrap -mx-3 mb-6">
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <Input type={"email"} placeholder={"رایانامه"} name={"email"} change={changeHandler}/>
                            </div>
                            <div className="w-full px-3 mt-2">
                                <Button
                                    variant={"primary"}
                                    disabled={isLoading}
                                    onClick={submitHandler}
                                    type="submit"
                                    className="w-full"
                                >
                                    تایید
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default ForgetPassword;
