import React, {useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import Input from "../../components/Form/Input";
import TitleSign from "../../components/Form/TitleSign/TitleSection";
import toast from "react-hot-toast";
import {UserResetPassword} from "../../services/UserService";
import Button from "../../components/Form";


const ResetPassword = () => {
    const [userData, setUserData] = useState({confirmPassword: null, password: null});
    const [isLoading, setIsLoading] = useState(false);
    const {token} = useParams();
    const navigate = useNavigate();

    const changeHandler = (e) => {
        setUserData({...userData, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!Object.values(userData).every(Boolean)) return toast.error("لطفا تمامی موارد را تکمیل نمایید.");
        setIsLoading(true);
        const promise = UserResetPassword(token, userData);
        toast.promise(promise, {
            loading: 'در حال انجام عملیات ...',
            success: () => {
                navigate("/signin", {replace: true});
                toast.success("می توانید با رمز عبور جدیدتان به حساب خود وارد شوید.")
                return `رمز عبور شما با موفقیت تغییر یافت.`;
            },
            error: (err) => {
                console.log(err);
                return `${err?.response?.data?.message ?? "عملیات انجام نشد"}`
            },
        },).finally(() => setIsLoading(false))
    };

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <TitleSign subTitle={"برای تغییر رمز عبور لطفا رمز عبور جدید خود را وارد نمایید."}/>
                    </div>
                    <form className="w-full max-w-lg">
                        <div className="flex flex-wrap -mx-3 mb-6">
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <Input type={"password"} placeholder={"رمز عبور جدید"} name={"password"} change={changeHandler}/>
                            </div>
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <Input type={"password"} placeholder={"تکرار رمز عبور جدید"} name={"confirmPassword"} change={changeHandler}/>
                            </div>
                            <div className="w-full px-3 mt-2">
                                <Button
                                    variant={"primary"}
                                    disabled={isLoading}
                                    onClick={submitHandler}
                                    type="submit"
                                    className="w-full"
                                >
                                    {/*<span className=" absolute left-0 inset-y-0 flex items-center pl-3"></span>*/}
                                    تایید
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default ResetPassword;
