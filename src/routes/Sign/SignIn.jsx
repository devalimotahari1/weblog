import React, {useState} from "react";
import {Link, useLocation, useNavigate} from "react-router-dom";
import Input from "../../components/Form/Input";
import TitleSign from "../../components/Form/TitleSign/TitleSection";
import toast from "react-hot-toast";
import {LoginUser} from "../../services/UserService";
import Button from "../../components/Form";
import {http} from "../../services/httpService";
import {useUserStore} from "../../context/user/provider";


const SignInPage = () => {
    const [userData, setUserData] = useState({email: null, password: null});
    const [isLoading, setIsLoading] = useState(false);
    const {setFullName} = useUserStore();
    const {state} = useLocation();
    const navigate = useNavigate();

    const changeHandler = (e) => {
        setUserData({...userData, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!Object.values(userData).every(Boolean)) return toast.error("لطفا تمامی موارد را تکمیل نمایید.");
        setIsLoading(true);
        const promise = LoginUser(userData);
        toast.promise(promise, {
            loading: 'در حال ورود ...',
            success: ({data}) => {
                http.setToken(data.token);
                setFullName(data.userFullName);
                navigate(state?.location ?? "/");
                return `${data.userFullName} خوش آمدید.`;
            },
            error: (err) => `${err?.response?.data?.message ?? "ورود انجام نشد"}`,
        },).finally(() => setIsLoading(false))
    };

    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <TitleSign subTitle={"همین حالا وارد حساب کاربری خودت شو و بلاگ جدیدتو شروع کن."}/>
                    </div>
                    <form className="w-full max-w-lg">
                        <div className="flex flex-wrap -mx-3 mb-6">
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <Input type={"email"} placeholder={"رایانامه"} name={"email"} change={changeHandler}/>
                            </div>
                            <div className="w-full md:w-1/1 px-3 mb-6 md:mb-0">
                                <Input type={"password"} placeholder={"رمز عبور"} name={"password"} change={changeHandler}/>
                                <Link to={"/forget-password"} passhref><a className="text-sm">رمز عبور خود را فراموش کردید؟</a></Link>
                            </div>
                            <div className="w-full px-3 mt-2">
                                <Button
                                    variant={"primary"}
                                    disabled={isLoading}
                                    onClick={submitHandler}
                                    type="submit"
                                    className="w-full"
                                >
                                    {/*<span className=" absolute left-0 inset-y-0 flex items-center pl-3"></span>*/}
                                    ورود
                                </Button>
                            </div>
                            <div className="w-full px-3 mt-5 text-center">
                                <Link to="/signup" className="text-center w-full">
                                    {/*<span className=" absolute left-0 inset-y-0 flex items-center pl-3"></span>*/}
                                    ساخت حساب جدید
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default SignInPage;
