import React, {useState} from "react";
import Input from "../../components/Form/Input";
import {Link, useNavigate} from "react-router-dom";
import TitleSign from "../../components/Form/TitleSign/TitleSection";
import {PostNewUser} from "../../services/UserService";
import toast from "react-hot-toast";
import Button from "../../components/Form";


const SignUpPage = () => {
    const [userData, setUserData] = useState({fullname: null, email: null, password: null, confirmPassword: null});
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const changeHandler = (e) => {
        setUserData({...userData, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!Object.values(userData).every(Boolean)) return toast.error("لطفا تمامی موارد را تکمیل نمایید.");
        setIsLoading(true);
        const promise = PostNewUser(userData);
        toast.promise(promise, {
            loading: 'در حال ثبت نام ...',
            success: ({data}) => {
                navigate("/signin")
                return `${data.message}`;
            },
            error: (err) => `${err?.response?.data?.message ?? "ثبت نام انجام نشد"}`,
        },).finally(() => setIsLoading(false))
    };
    return (
        <>
            <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <TitleSign/>
                    </div>
                    <form className="w-full max-w-lg">
                        <div className="flex flex-wrap -mx-3 mb-6">
                            <div className="w-full px-3 mb-6 md:mb-0">
                                <Input
                                    type={"text"}
                                    change={changeHandler}
                                    placeholder={"نام و نام خانوادگی"}
                                    name={"fullname"}
                                />
                            </div>
                            <div className="w-full px-3 mb-6 md:mb-0">
                                <Input
                                    type={"email"}
                                    change={changeHandler}
                                    placeholder={"رایانامه"}
                                    name={"email"}
                                />
                            </div>
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                <Input
                                    type={"password"}
                                    change={changeHandler}
                                    placeholder={"رمز عبور"}
                                    name={"password"}
                                />
                            </div>
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                <Input
                                    type={"password"}
                                    change={changeHandler}
                                    placeholder={" تکرار رمز عبور"}
                                    name={"confirmPassword"}
                                />
                            </div>
                            <div className="w-full px-3 mt-2">
                                <Button
                                    disabled={isLoading}
                                    onClick={submitHandler}
                                    type="submit"
                                    variant={"primary"}
                                    className="w-full"
                                >
                                    <span className=" absolute left-0 inset-y-0 flex items-center pl-3"></span>
                                    ثبت نام
                                </Button>
                            </div>
                            <div className="w-full px-3 mt-5">
                                <Link to="/signin">
                                    <button className="text-primary w-full">
                                        <span className=" absolute left-0 inset-y-0 flex items-center pl-3"></span>
                                        حساب دارم
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default SignUpPage;
