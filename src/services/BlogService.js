import {http} from "./httpService";
// GET ONE
export const getOneBlog = (postID) => {
  return http.get(`/post/${postID}`);
};
// GET ALL
export const getAllBlog = () => {
  return http.get(`/`);
};
//DELETE
export const deleteBlog = (postID) => {
  return http.delete(`/posts/${postID}`);
};
export default getAllBlog;
