import {http} from "./httpService";

// GET ONE
export const PostNewUser = async (data) => {
    return await http.post("/users/register", data);
};
export const LoginUser = async (data) => {
    return await http.post("/users/login", data);
};
export const UserForgetPassword = async (data) => {
    return await http.post("/users/forget-password", data);
};
export const UserResetPassword = async (token, data) => {
    return await http.post(`/users/reset-password/${token}`, data);
};
export const LoginWithToken = async (token) => {
    return await http.post("/users/login/token", {token});
};
