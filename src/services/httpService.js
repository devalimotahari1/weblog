import axios from "axios";
import toast from "react-hot-toast";


const BASE_URL = "http://localhost:5000/";

axios.defaults.baseURL = BASE_URL;
axios.defaults.headers["Access-Control-Allow-Origin"] = "*";

axios.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.code?.includes("ERR_NETWORK")) {
            toast.error(
                "اتصال به سرور ممکن نیست. لطفا از اتصال اینترنت و VPN خود اطمینان حاصل نمایید."
            );
        }
        return Promise.reject(error);
    }
);

const setToken = (token) => {
    if (token) {
        axios.defaults.headers.authorization = `Bearer ${token}`;
        localStorage.setItem("jwt_access_token", JSON.stringify(token));
    }
};

const removeToken = () => {
    axios.defaults.headers.authorization = undefined;
    localStorage.removeItem("jwt_access_token");
};

const getToken = () => JSON.parse(localStorage.getItem("jwt_access_token"));
export const http = {
    get: axios.get,
    post: axios.post,
    delete: axios.delete,
    put: axios.put,
    BASE_URL,
    setToken,
    getToken,
    removeToken,
};